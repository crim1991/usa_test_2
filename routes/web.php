<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FeedbackController@index');

Route::post('/save-feedback', 'FeedbackController@saveFeedback');
Route::get('/get-next-feedback/{from}', 'FeedbackController@getNextFeedback');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/change-status/{id}', 'HomeController@changeStatus');
Route::get('/edit-feedback/{id}', 'HomeController@edit');
Route::get('/update-feedback/{id}', 'HomeController@update');
Route::post('/delete-feedback/{id}', 'HomeController@deleteFeedback');
Route::post('/sort-feedback', 'HomeController@sortFeedback');

//User Login

Route::get('login/github', 'LoginController@redirectToProvider');
Route::get('login/github/callback', 'LoginController@handleProviderCallback');
