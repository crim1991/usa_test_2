var FeedbackFormValidation = (function (global) {

	function addMethods() {
		$.validator.addMethod("correctMessage", function(value, element) {
			return this.optional(element) || /^.{1,1000}$/.test(value);
		}, "Message should have max 1000 characters");

		$.validator.addMethod("correctEmail", function(value, element) {
			return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
		}, "Email should have next format: example@email.com");

		$.validator.addMethod("correctName", function(value, element) {
			return this.optional(element) || /^[a-zA-Z ]{3,50}$/.test(value);
		}, "Name should contain only letters, from 3 to 50");
	}

	function formRules(form) {
		$(form).validate({
			rules: {
				name: {
					required: true,
					correctName: true
				},
				email: {
					required: true,
					correctEmail: true
				},
				message: {
					required: true,
					correctMessage: true
				}
			},
			messages: {
				name: {
					required: "Name is required"
				},
				email: {
					required: "Email is required",
				},
				message: {
					required: "Message is required",
				}
			}
		});
	}

	function init() {
		addMethods();
		formRules("#feedback_form");
	}

	return {
		init: init
	}

})(window);

FeedbackFormValidation.init();

var FeedbackReadMore = (function (global) {
	function getNextFeedback() {
		var from = $('.feedback_wrapper').find('.card').length;
		$.ajax({
			url: '/get-next-feedback/' + from,
			method: 'GET'
		}).done(function (feedback) {
			var feedback = JSON.parse(feedback);
			insertFeedback(feedback);
			if (feedback.length < 10) {
				$('.read_btn').addClass('d-none');
			}
		}).fail(function () {
			console.log('ERROR');
		});
	}

	function insertFeedback(feedback) {
		$.each(feedback, function (index, value) {
			var html = '<div class="card mt-2">' +
				'<div class="card-body">' +
				'<h5 class="card-title">'+value.name+'</h5>' +
				'<h6 class="card-title">'+value.email+'</h6>' +
				'<p class="card-text">'+value.message+'</p>';

			if (value.photo) {
				html += '<img src="../uploads/feedback_photos/'+value.photo+'"/>';
			}
			html += '</div></div>';
			$('.feedback_wrapper').find('.card:last-child').after(html);
		});
	}

	function init() {
		$('.read_btn').on('click', getNextFeedback);
	}

	return {
		init: init
	}

})(window);

FeedbackReadMore.init();
