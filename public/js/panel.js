$(function () {
	//Laravel POST token
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('.status_btns a').on('click', function () {
		var feedback_id = $(this).parents().eq(1).attr('id');
		feedback_id = feedback_id.slice(5, 6);
		var hide = 'fa-eye-slash';
		var show = 'fa-eye';
		var button = $(this).find('i');

		var hidden = button.hasClass(hide);

		if (hidden) {
			button.removeClass(hide).addClass(show);
			changeStatus(1, feedback_id);
		} else {
			button.removeClass(show).addClass(hide);
			changeStatus(0, feedback_id);
		}
	});

	function changeStatus(status, feedback_id) {
		$.ajax({
			url: '/change-status/' + feedback_id,
			method: 'POST',
			data: {
				active: status
			}
		});
	}

	$("#sortable").sortable({
		update: function( event, ui ) {
			var data = $(this).sortable('serialize');
			$.ajax({
				url: '/sort-feedback',
				method: 'POST',
				data: data
			});
		}
	});

	$( "#sortable" ).disableSelection();
});