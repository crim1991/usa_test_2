<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Feedback;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class FeedbackController extends Controller
{
	protected $path;

	public function __construct()
	{
		$this->path = 'uploads/feedback_photos/';
	}

	public function index()
	{
		if (Session::has('client_user')) {
			$client_user = Session::get('client_user');
		} else {
			return Redirect::to('/login/github');
		}

		$feedbacks = Feedback::where('active', 1)
					->orderBy('order', 'ASC')->orderBy('id', 'DESC')
					->limit(10)->get();
		$feed_count = $feedbacks->count();

		return view('feedback.index', compact('feedbacks', 'feed_count', 'client_user'));
    }

	public function saveFeedback(FeedbackRequest $request)
	{
		$data = $request->except('_token', 'photo');

		Feedback::create($data);

		Session::flash('success', 'Thank you for leaving a feedback');

		return Redirect::back();
    }

	public function getNextFeedback($from)
	{
		$amount = 10;
		$result = Feedback::select('id', 'name', 'email', 'message', 'photo')
			->where('active', 1)
			->latest()
			->skip($from)
			->take($amount)
			->get();

		return json_encode($result->toArray());
	}
}
