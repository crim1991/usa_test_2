<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Requests\FeedbackUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    	$feedbacks = Feedback::orderBy('order', 'ASC')->orderBy('id', 'DESC')->get();
        return view('home', compact('feedbacks'));
    }

	public function changeStatus(Request $request, $id)
	{
		Feedback::where('id', $id)->update($request->all());
    }

	public function sortFeedback(Request $request)
	{
		$position = 1;
		foreach ($request->get('item') as $item_id) {
			Feedback::where('id', $item_id)->update(['order' => $position]);
			$position++;
		}
    }

	public function edit($id)
	{
		$feedback = Feedback::where('id', $id)->first();

    	return view('edit', compact('feedback'));
	}

	public function update(FeedbackUpdateRequest $request, $id)
	{
		Feedback::where('id', $id)->update($request->all());
		Session::flash('success', 'Feedback updated with success.');
		return Redirect::back();
	}

	public function deleteFeedback($id)
	{
		Feedback::destroy($id);
		Session::flash('success', 'Feedback deleted.');
		return Redirect::back();
    }
}
