<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::table('users')
			->insert([
				'id' => 1000,
				'name' => 'admin',
				'email' => 'admin@mail.com',
				'password' => '$2y$10$8P31UaDio3RWBKSlc74wguov8UA.lmshFlmYSfMgKa.T/quMI1H26' //123123123
			]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::table('users')->where('id', 1000)->delete();
    }
}
