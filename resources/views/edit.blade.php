@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12">
				@if(Session::has('success'))
					<div class="alert alert-success">
						<p>{{ Session::get('success') }}</p>
					</div>
				@endif
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<div>
					<a href="{{ url('home') }}"><i class="fa fa-arrow-left"></i> Back</a>
				</div>
				<form id="feedback_form" action="{{ url("/update-feedback/$feedback->id") }}" method="HEAD"
					  enctype="multipart/form-data">
					<div class="form-group">
						<label for="name">Name: {{ $feedback->name }}</label>
					</div>

					<div class="form-group">
						<label for="email">Email: {{ $feedback->email }}</label>
					</div>

					<div class="form-group">
						<label for="message">Message <span class="required">*</span></label>
						<textarea name="message" id="message" class="form-control" style="resize: none;"
								  rows="7">{{ $feedback->message }}</textarea>
					</div>

					<input type="hidden" name="edit" value="1">

					<div class="form-group text-right">
						<button class="btn btn-success" type="submit">Edit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
