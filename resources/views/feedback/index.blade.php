<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Feedback</title>
	<link rel="stylesheet" href="{{ asset('css/libs/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				@if(Session::has('success'))
					<div class="alert alert-success">
						<p>{{ Session::get('success') }}</p>
					</div>
				@endif
				<h3>Leave your feedback here</h3>
				@if ($errors->any())
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				@endif
				<form id="feedback_form" action="{{ url('/save-feedback') }}" method="POST"
					  enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="name">Name: {{ $client_user->nickname }}</label>
						<input type="hidden" name="name" class="form-control"
							   value="{{ $client_user->nickname }}">
					</div>

					<div class="form-group">
						<label for="email">Email: {{ $client_user->email }}</label>
						<input type="hidden" name="email"
							   class="form-control"
							   value="{{ $client_user->email }}"/>
					</div>

					<div class="form-group">
						<label for="photo">Avatar:</label>
						<div>
							<img src="{{ $client_user->avatar }}" alt="Avatar">
						</div>
					</div>

					<div class="form-group">
						<label for="message">Message <span class="required">*</span></label>
						<textarea name="message" id="message" class="form-control" style="resize: none;"
								  rows="7"></textarea>
					</div>

					<div class="form-group text-right">
						<button class="btn btn-success" type="submit">Send</button>
					</div>
				</form>
			</div>
		</div>
		@if($feed_count)
			<div class="row">
				<div class="col-6 offset-3">
					<h3>Read feedback</h3>
					<div class="feedback_wrapper">
						@foreach($feedbacks as $feedback)
							<div class="card mt-2">
								<div class="card-body">
									<h5 class="card-title">{{ $feedback->name }}</h5>
									<h6 class="card-title">{{ $feedback->email }}</h6>
									<p class="card-text">{{ $feedback->message }}</p>
									@if($feedback->photo)
										<img src="{{ asset("uploads/feedback_photos/$feedback->photo") }}" alt="Photo">
									@endif
									@if($feedback->edit)
										<span class="float-right"><i>edited</i></span>
									@endif
								</div>
							</div>
						@endforeach
					</div>
					@if($feed_count >= 10)
						<div class="mt-3 text-center">
							<button type="button" class="btn btn-primary read_btn">Read more</button>
						</div>
					@endif
				</div>
			</div>
		@endif
	</div>
	<script src="{{ asset('js/libs/jquery.min.js') }}"></script>
	<script src="{{ asset('js/libs/bootstrap.min.js') }}"></script>
	<script src="{{ asset('js/libs/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('js/libs/additional-methods.min.js') }}"></script>
	<script src="{{ asset('js/custom.js') }}"></script>
</body>
</html>