@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Feedback</div>

                <div class="card-body">
					@if(Session::has('success'))
						<div class="alert alert-success">
							<p>{{ Session::get('success') }}</p>
						</div>
					@endif
					@if($feedbacks->count())
					<table class="table table-striped text-center">
						<thead>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Message</th>
								<th>Edited</th>
								<th>Status</th>
								<th>Options</th>
							</tr>
						</thead>
						<tbody id="sortable" class="ui-sortable">
							@foreach($feedbacks as $feedback)
								<tr id="item_{{ $feedback->id }}">
									<td>{{ $feedback->id }}</td>
									<td>{{ $feedback->name }}</td>
									<td>{{ $feedback->email }}</td>
									<td>{{ $feedback->message }}</td>
									<td>
										@if($feedback->edit)
											edited
										@else
											none
										@endif
									</td>
									<td class="status_btns">
										@if($feedback->active)
											<a>
												<i class="fa fa-eye" aria-hidden="true"></i>
											</a>
										@else
											<a>
												<i class="fa fa-eye-slash" aria-hidden="true"></i>
											</a>
										@endif
									</td>
									<td class="actions">
										<a class="text-primary" href="{{ url("/edit-feedback/$feedback->id") }}">
											<i class="fa fa-edit"></i></a>
										<form action="{{ url("/delete-feedback/$feedback->id") }}" method="POST">
											@csrf
											<button type="submit" class="btn text-danger"><i class="fa
											fa-trash"></i></button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					@else
						<p class="text-primary">No feedback yet.</p>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
